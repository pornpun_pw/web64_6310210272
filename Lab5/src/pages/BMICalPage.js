import BMIResult from "../components/BMIResult";
import { useState } from "react";
import Button from '@mui/material/Button';
import  Grid  from '@mui/material/Grid';
import { Typography,Box, Container } from "@mui/material";



function BMICalPage(){
      
    const [ name,setName ] = useState("");
    const [ bmiResult ,setBmiResult ] = useState("");
    const [ translateResuit , setTransteresult ]  
                                         =useState(""); 
    
    const [ height, setHeight] = useState("");
    const [ weight, setWeight] = useState("");

    function CalculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h*h);
        setBmiResult ( bmi);
        if (bmi > 25){
            setTransteresult("เยี่ยมมมมมม");
         } else{
                setTransteresult("ยอดเยี่ยมมมมม");
            }
    }
    
return(
            <Container maxWidth='lg'>
            <Grid container spacing={2} sx= {{ marginTop :"10px"}}>
                <Grid item xs={12}>
                  <Typography variant="h5">
                      ยินดีต้อนรับสู่เว็บคำนวณ BMI
                  </Typography>
                </Grid>
                <Grid item xs={8}>
                <Box sx={{textAlign :'left'}}>
                        คุณชื่อ:<input type="text" 
                                    value={name}
                                    onChange={(e) =>{ setName(e.target.value); } } />
                                     <br />
                                     <br />
                        สูง:<input type="text"
                                    value={ height }
                                    onChange={(e) =>{ setHeight(e.target.value); } } />  
                                    <br />
                                    <br />
                        น้ำหนัก:<input type="text"
                                    value={ weight }
                                    onChange={(e) =>{ setWeight(e.target.value); } } /> 
                                    <br />  
                                    <br />
                <Button variant="contained"  onClick={ () =>{ CalculateBMI()}}>
                     Calculate
                </Button>
                </Box>
                </Grid>
                <Grid item xs={4}>
                { bmiResult !=0 &&
                    <div>
                            <hr />  
                            นี่ผลการคำนวณจ้า
                            <BMIResult
                                name = { name }
                                bmi = { bmiResult }
                                result = { translateResuit }
                            
                            />
                            </div>
                }             
                </Grid>
            </Grid>
            </Container>
    
    );
}
export default BMICalPage;
/*<div align="left">
             <div align="center">
                
                 ยินดีต้อนรับสู้เว็บคำนวณ BMI
                 <hr />

                 คุณชื่อ:<input type="text" 
                              value={name}
                              onChange={(e) =>{ setName(e.target.value); } } /> <br />
                 สูง:<input type="text"
                              value={ height }
                              onChange={(e) =>{ setHeight(e.target.value); } } />  <br />
                 น้ำหนัก:<input type="text"
                              value={ weight }
                              onChange={(e) =>{ setWeight(e.target.value); } } />  <br />  
                 
                 
                 <Button variant="contained"  onClick={ () =>{ CalculateBMI()}}>
                     Calculate
                 </Button>
                
                { bmiResult !=0 &&
                    <div>
                    <hr />  
                    นี่ผลการคำนวณจ้า
                    <BMIResult
                        name = { name }
                        bmi = { bmiResult }
                        result = { translateResuit }
                      
                    />
                    </div>
                }                                                      
             </div>

         </div>
    );
}
export default BMICalPage;*/