import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Name from './components/Name';

function App() {
  return (
    <div className="App">
      <Header />
      <Name /> 
      <Body />
      <Footer />
    </div>
  );
}

export default App;
