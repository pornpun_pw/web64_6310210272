import AboutUs from "../components/AboutUs";


function AboutUsPage (){

     return(
        <div>
            <div align="center">
                <h2>คณะผู้จัดทำเว็บนี้</h2>
                
                <AboutUs name="พรพรรณ" 
                         address="บ้านโคกสัก" 
                         province="ควนโส"/>
                
                <AboutUs name="วิไลยรัตน์" 
                         address="ควนโส" 
                         province="ควนเนียง"/>
                
                <AboutUs name="วิไลยรัตน์" 
                         address="ควนโส" 
                         province="ควนเนียง"/>
                
            </div>
        </div>

     );
}
export default AboutUsPage;