const express = require('express')
const req = require('express/lib/request')
const app = express()
const port = 4000


app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res) => {
    
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if ( !isNaN(weight) && !isNaN(height)){
        let bmi = weight / (height * height)
        result ={
            "status" :  200,
            "bmi" : bmi
        }
    }else{
        result = {
             "status" : 400,
             "message" : "Weight or Height is not a number"
        }
    }
        res.send(JSON.stringify(result))   
})

app.get('/triangle',(req, res) =>{
  let high = parseFloat(req.query.high )
  let base = parseFloat(req.query.base)
  var result = {}
  if( !isNaN(high) && !isNaN(base)){
      let triangle = 0.5 * base * high
      result = {
          "status" : 200,
          "triangle" : triangle
      }
  }else {

      result = {
          "status" : 400,
          "message" : "High or Base is not a number"
      }
  }
  res.send(JSON.stringify(result))

})

app.post('/score', (req, res) => {
  let name = ''
  let score = parseFloat(req.query.score)
  var result = {}

  if ( !isNaN(score)){
      if (score <= 100 && score >= 80){
          res.send( ' Grade is A')
      }else if (score >= 75 && score < 80){
          res.send( ' Grade is B+')
      }else if (score >= 70 && score < 75){
          res.send(' Grade is B')
      }else if (score >= 65 && score < 70){
          res.send( ' Grade is C+')
      }else if (score >= 60 && score < 65){
          res.send(' Grade is C')
      }else if (score >= 55 && score < 60){
          res.send( ' Grade is D+')
      }else if (score >= 50 && score < 55){
          res.send(' Grade is B')
      }else if (score >= 0 && score < 50){
          res.send( ' Grade is E')
      }else{
          res.send('unable to calculate')
      }   
  }else {

      result = {
          "status" : 400,
          "message" : "score is not a number"
      }
  }

  res.send(JSON.stringify(result))
})


app.get('/hello/', (req, res) => {
    res.send('Sawasdee '+req.query.name)
  })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})